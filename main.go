package main

import (
	"context"
	"fmt"
	"gitlab.com/atharshaikh/pkgs-cli-go/pkgs_proto_go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func main() {
	conn, err := grpc.Dial(":9000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()

	dists := []string{"stable", "testing", "unstable", "oldstable"}
	client := pkgs_proto_go.NewDBPackageQueryClient(conn)
	for _, i := range dists {
		response, err := client.FetchPackageDetails(context.Background(), &pkgs_proto_go.PackageQuery{
			Query: "golang-1.19",
			Dist:  i,
		})
		if err != nil {
			fmt.Printf("Error when calling FetchPackageDetails: %s\n", err)
			continue
		}
		fmt.Printf("Version: %s \t Repo: %s \t Dist: %s", response.Version, response.Repo, i)
		fmt.Println()
	}
}
